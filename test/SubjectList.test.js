const SubjectList = artifacts.require('SubjectList')

contract('SubjectList',(account)=>{
    beforeEach(async ()=>{
        this.subjectList = await SubjectList.deployed()
    })

    it('subjectList contract deploy successfully',async()=>{
        const address = await this.subjectList.address
        isValidAddress(address)
    })


    it('adding subject',async()=>{
        return SubjectList.deployed().then((instance)=>{
            s= instance;
            _code = "CSB203"
            return s.createSubject(_code,"DApp");
        }).then((transaction)=>{
            isValidAddress(transaction.tx);
            isValidAddress(transaction.receipt.blockHash);
            return s.subjectCount();
        }).then((count)=>{
            assert.equal(count,1);
            return s.subjects(1)
        }).then((subject)=>{
            assert.equal(subject.code,_code)
        })
    })


    it("test find subject",async()=>{
        return SubjectList.deployed().then((instance)=>{
            s = instance
            return s.createSubject("123","math").then(async(tx)=>{
                return s.createSubject("1234","science").then(async(tx)=>{
                    return s.createSubject("12345","bio").then(async(tx)=>{
                        return s.createSubject("123456","geo").then(async(tx)=>{
                            return s.createSubject("123","his").then(async(tx)=>{
                                return s.subjectCount().then(async(count)=>{
                                    assert.equal(count,6)
                                    return s.fineSubject(4).then((subject)=>{
                                        assert.equal(subject.name,"bio")
                                    })
                                })
                            })
                        })
                    })
                })
            })
        })
    })

    it('text marking retired',async()=>{
        return SubjectList.deployed().then((instance)=>{
            s = instance
            return s.fineSubject(1).then((osubject)=>{
                assert.equal(osubject.name,"DApp")
                assert.equal(osubject.retired,false)
                return s.markRetired(1).then(async(transaction)=>{
                    return s.fineSubject(1).then(async(nsubject)=>{
                        assert.equal(nsubject.name,"DApp")
                        assert.equal(nsubject.retired,true)
                    })
                })
            })
        })
    })
})

function isValidAddress(address){
    assert.notEqual(address, 0x0)
    assert.notEqual(address,'')
    assert.notEqual(address,null)
    assert.notEqual(address,undefined)
}











// const SubjectList = artifacts.require('SubjectList')

// contract('SubjectList',(account)=>{
//     let subjectList
//     beforeEach(async ()=>{
//         subjectList = await SubjectList.deployed()
//     })

//     it('subjectList contract deploy successfully',async()=>{
//         const address = await subjectList.address
//         isValidAddress(address)
//     })



//     describe('subject',async()=>{
//         let result;
//         let _code = 'CSB203';
//         before(async()=>{
//           result = await subjectList.createSubject(_code,'DApp')
//         //   console.log("_code "+_code)
//         })
//         it("add subject",async()=>{
//           const event = result.logs[0].args
//         //   console.log(event)
//           assert.equal(event._id.toNumber(),1,'id is correct')
//           assert.equal(event.code,_code,"code is correct")
//           assert.equal(event.name,"DApp", "name is correct")
//           assert.equal(event.retired,false,"retired is correct")
    
//         // await decentragram.uploadImage('',"image description",{from:author}).should.be.rejected
    
    
//         })
//         it("update subject",async()=>{
//           result = await subjectList.createSubject("123123",'sdfafdafd')
//           data = result.logs[0].args;
//           assert.equal(data._id.toNumber(),2,"id is correct");
//           assert.equal(data.code,'123123',"code is correct")
//           assert.equal(data.name,"sdfafdafd","name is correct")
//           assert.equal(data.retired,false,"retired is correct")

//           return subjectList.updateSubject(data._id.toNumber(),"ELE203","cyber").then(async(nSubject)=>{
//             neData = nSubject.logs[0].args
//             assert.equal(neData._id.toNumber(),2,"id is correct");
//             assert.equal(neData.code,'ELE203',"code is correct")
//             assert.equal(neData.name,"cyber","name is correct")
//             assert.equal(neData.retired,false,"retired is correct")


//           })


//         })
    
       
    
    
        
//     })


    

    
// })

// function isValidAddress(address){
//     assert.notEqual(address, 0x0)
//     assert.notEqual(address,'')
//     assert.notEqual(address,null)
//     assert.notEqual(address,undefined)
// }

