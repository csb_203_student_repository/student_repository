// // import smart contracts
// const StudentList = artifacts.require('StudentList')

// // creating the contract where we can code for test any function
// contract('StudentList', (account) => {


//     beforeEach(async () => {
//         this.studentList = await StudentList.deployed()
//     })


//     // <<<it>> is the mocha testing function that come together with truffle
//     it('deploys successfully', async () => {
//         const address = await this.studentList.address
//         isValidAddress(address)
//     })



//     it('test adding student', async()=>{
//         return StudentList.deployed().then((instance)=>{
//             s = instance;
//             studentCid = 1;
//             return s.createStudent(studentCid,"Ugyen Lhendup");
//         }).then((transaction)=>{
//             isValidAddress(transaction.tx)
//             isValidAddress(transaction.receipt.blockHash);
//             return s.studentCount()
//         }).then((count)=>{
//             assert.equal(count,1);
//             return s.students(1)
//         }).then((student)=>{
//             assert.equal(student.cid,studentCid)
//         })
//     })

//     it('text findind student',async()=>{
//         return StudentList.deployed().then(async(instance)=>{
//             s= instance;
//             studentCid = 2;
//             return s.createStudent(studentCid++,"thsering yangzom").then(async(tx)=>{
//                 return s.createStudent(studentCid++,"dorji tshering").then(async(tx)=>{
//                     return s.createStudent(studentCid++,"chimi dema").then(async(tx)=>{
//                         return s.createStudent(studentCid++,"tashi phuntsho").then(async(tx)=>{
//                             return s.createStudent(studentCid++,"samten zangmo").then(async(tx)=>{
//                                 return s.createStudent(studentCid++,"sonam chophel").then(async(tx)=>{
//                                     return s.studentCount().then(async(count)=>{
//                                         assert.equal(count,7)
//                                         return s.findStudent(5).then(async(student)=>{
//                                             assert.equal(student.name,"tashi phuntsho")
//                                         })
//                                     })
//                                 })
//                             })
//                         })
//                     })
//                 })
//             })
//         })

//     })

//     it("test marking gradute",async()=>{
//         return StudentList.deployed().then(async(instance)=>{
//             s = instance
//             return s.findStudent(1).then(async(ostudent)=>{
//                 assert.equal(ostudent.name, "Ugyen Lhendup")
//                 assert.equal(ostudent.graduated,false)
//                 return s.markGraduated(1).then(async(transaction)=>{
//                     return s.findStudent(1).then(async(nstudent)=>{
//                         assert.equal(nstudent.name, "Ugyen Lhendup")
//                         assert.equal(nstudent.graduated,true)
//                     })
//                 })
//             })
//         })

//     })
// })

// function isValidAddress(address){
//     assert.notEqual(address, 0x0)
//     assert.notEqual(address,'')
//     assert.notEqual(address,null)
//     assert.notEqual(address,undefined)
// }