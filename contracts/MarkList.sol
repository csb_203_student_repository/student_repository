// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

contract MarkList{
    uint public marksCount = 0;
    enum Grades{A,B,C,D,FAIL}
    struct Marks{
        uint cid;
        string code;
        Grades grades;
    }

    mapping(uint=>mapping(string=>Marks)) public marks;
    event addMarksEvent(
        uint indexed cid,
        string indexed code,
        Grades grades
    );
    event updateMarksEvent(
        uint indexed cid,
        string indexed code,
        Grades grades
    );

    function addMarks(uint _cid,string memory _code,Grades _grade) public {
        require(bytes(marks[_cid][_code].code).length==0,"Marks not found");
        marksCount++;
        marks[_cid][_code]=Marks(_cid,_code,_grade);
        emit addMarksEvent(_cid, _code,_grade);
    }

    function findMarks(uint _cid,string memory _code) public view returns(Marks memory){
        return marks[_cid][_code];
    }


    function updateMarks(uint _cid,string memory _code,Grades _grade)public{
        require(bytes(marks[_cid][_code].code).length!=0,'marks are not graded');
        marks[_cid][_code]=Marks(_cid,_code,_grade);
        emit updateMarksEvent(_cid, _code, _grade);
        // return marks[_cid][_code];
    }
}
