// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract SubjectList{
    uint public subjectCount=0;

    struct Subject{
        uint _id;
        string code;
        string name;
        bool retired;
    }

    event createSubjectEvent(
        uint _id,
        string  code,
        string name,
        bool retired);
    
    event markGraduatedEvent(uint indexed id);


    mapping(uint=>Subject) public subjects;

    function createSubject(string memory _code,string memory _name) public returns(Subject memory){
        subjectCount++;
        subjects[subjectCount]=Subject(subjectCount,_code,_name,false);
        emit createSubjectEvent(subjectCount, _code, _name, false);
        return subjects[subjectCount];
    }

    function markRetired(uint _id) public returns(Subject memory){
        subjects[_id].retired=true;
        emit markGraduatedEvent(_id);
        return subjects[_id];

    }

    function fineSubject(uint _id) public view returns(Subject memory) {
        return(subjects[_id]);
        
    }


    event updateSubjectEvent(
        uint _id,
        string  code,
        string name,
        bool retired
    );


    function updateSubject(uint _id, string memory _code, string memory _name) public returns(Subject memory) {
        subjects[_id] = Subject({
            _id:_id,
            code:_code,
            name:_name,
            retired:false
        });
        emit updateSubjectEvent(_id, _code, _name, false);
        return subjects[_id];
    }
}