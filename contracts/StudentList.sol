// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract StudentList{
    uint public studentCount = 0;

    constructor(){
        // createStudent(1001, "tshewang peljor wangchuk");
    }
    // model a student
    struct Student{
        uint _id;
        uint cid;
        string name;
        bool graduated;
    }
    // creating storage for student data
    mapping(uint=>Student) public students;

    // event 
    event createStudebtEvent(
        uint _id,
        uint indexed cid,
        string name,
        bool graduated
    );

    // event for graduation status
    event markGraduatedEvent(
        uint indexed cid
    );






    function createStudent(uint _studentCid, string memory name) public returns(Student memory) {
        studentCount++;
        students[studentCount] = Student(studentCount,_studentCid,name,false);
        emit createStudebtEvent(studentCount, _studentCid, name, false);
        return students[studentCount];
    }

    function markGraduated(uint _id) public returns (Student memory){
        students[_id].graduated = true;
        // trigger create event
        emit markGraduatedEvent(_id);
        return students[_id];
    }


    // fetch student information from storage
    function findStudent(uint _id) public view returns(Student memory){
        return students[_id];
    }
}