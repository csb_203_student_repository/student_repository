// import Form from './component/Form';
import React,{Component} from 'react';
import Web3 from 'web3'
import { STUDENT_LIST_ABI,STUDENT_LIST_ADDRESS } from './abi/config_studentList';
// import '.App.css'

class App extends  Component{
 componentDidMount(){
  if(!window.ethereum)
    throw new Error("no crypto wallet fount , please install")
  window.ethereum.send("eth_requestedAccount")
  this.loadBlockchainData()
 }
 async loadBlockchainData(){
  const web3 = new Web3(Web3.givenProvider || "http//:localhost:8545")
  const account = await web3.eth.getAccounts()
  this.setState({account:account[0]})
  const studentList = new web3.eth.Contract(
    STUDENT_LIST_ABI,STUDENT_LIST_ADDRESS
  )
  this.setState({studentList})
  const studentCount = await studentList.methods.studentCount().call()
  this.setState({studentCount})
  this.state.students=[]
  for (var i = 1;i<studentCount;i++){
    const student = await studentList.methods.students(i).call()
    this.setState({
      students:[...this.state.students,student]
    })

  }
 }
 constructor(props){
  super(props)
  this.state = {
    account : '',
    studentCount:0,
    students:[]

  }
 }
 render(){
  return(
    <div className='container'>
      <h1>Student Marks Management</h1>
      <p1>your account:{this.state.account}</p1>
      <ul id='studentList' className='list-unstyled'>
        {
          this.state.students.map((student,key)=>{
            return(
              <li className='list-group-item checkbox' key={key}>
              <span className='name alert'>
                {student._id}. {student.cid}{student.name}
              </span>
              <input className='from-check-input' type='checkbox' name ={student._id} defaultChecked = {student.graduated} disabled={student.graduated} ref={(input)=>{
                this.checkbox = input
              }} />
              <label className='fom-check-label'>Graduated</label>

            </li>
            )
            
          })
        }

      </ul>
      <span>Total Student : {this.state.studentCount}</span>
    </div>

  )
  
 }
}

export default App;
